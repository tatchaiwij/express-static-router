const express = require('express');
const app = express();
const port = 3000;

app.use(express.static('public'))

app.get('/', (req, res) => {
   let a = String(req.query.a);
   let b = String(req.query.b);
   res.send(`${a + b}`);
});

app.listen(port, () => {
  console.log(`Listening at http://localhost:${port}`);
});
