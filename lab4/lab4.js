const express = require('express');
const bodyParser = require('body-parser')
const app = express();
const port = 3000;

app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

app.post('/', (req, res) => {
    const nums = []
    nums.push(Number(req.body.a));
    nums.push(Number(req.body.b));
    nums.push(Number(req.body.c));
    nums.push(Number(req.body.d));
    nums.push(Number(req.body.e));
    nums.push(Number(req.body.f));
    nums.push(Number(req.body.g));
    nums.push(Number(req.body.h));
    nums.push(Number(req.body.i));
    nums.push(Number(req.body.j));

    const resnum = [];
    const oe = [];

    nums.forEach(num => {
        if (num > 99) {
            resnum.push(num);
        }
        if (num == 0) {
            oe.push(` ${num} is Zero`);
        } else if (num % 2 == 0){
            oe.push(` ${num} is Even`);
        } else {
            oe.push(` ${num} is Odd`);
        }
    });

    if (resnum.length !== 0) {
        res.send(
            resnum.map(numout =>
                `<p>${numout}</p>`
            ).join('')
       )
    } else {
        res.send(
            oe.map(status =>
                `<p>${status}</p>`
            ).join('')
        )
    }
});

app.listen(port, () => {
    console.log(`Listening at http://localhost:${port}`);
});
