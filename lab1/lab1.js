const express = require('express');
const app = express();
const port = 3000;
var path = require('path');

app.use(express.static('Lab'));

app.use('/Lab', express.static('Lab'))

app.listen(port, () => {
   console.log(`Listening at http://localhost:${port}`);
});
